#!/bin/bash

# Check the states of all test runs.

running_ids=$(sqlite3 benchmarks.db "SELECT job_id FROM benchmarks")
for id in $running_ids
do
    state=$(sacct --format State -j $id | tail -n 1 | awk '{print $1}')
    case $state in
        "RUNNING")
            true;;
        "PENDING")
            true;;
        "COMPLETING")
            true;;
        "TIMEOUT")
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'TIMEOUT' WHERE job_id = $id";;
        "CANCELLED")
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'CANCELLED' WHERE job_id = $id";;
        "COMPLETED")
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'COMPLETED' WHERE job_id = $id";;
        "FAILED")
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'FAILED' WHERE job_id = $id";;
        "OUT_OF_ME+")
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'OUT_OF_MEMORY' WHERE job_id = $id";;
        *)
            sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'UNKNOWN' WHERE job_id = $id";;
    esac
done
