# Cluster Experiments Tools

A collection of scripts to run large scale benchmarking experiments on a SLURM cluster.

## Quick start
Clone this repository on the server and follow [the steps below](#how-to-run-benchmarks-and-get-the-results).

If you have access to the NUC cluster and you don't have a LUNARC account yet or don't know how to login to the cluster, have a look at the [SETUP.md](SETUP.md) file.

## If you run on a different cluster
Change the cluster and partition names in `runner.sh` for the sbatch command as needed.

## How to run benchmarks and get the results

1. Create one or more SLURM batch scripts to run. See `/examples` or `template.sh` for how these scripts look like. You can test you script for an instance by running
```bash
./dry_run.sh script.sh instance
```
2. Initialize the database by running
```bash
./init.sh
```
3. Add instances and scripts to run the instances to the database
```bash
./add.sh script.sh path/to/instances/ [pattern]
# for example
./add.sh script.sh ~/instances/ *.opb
```
4. Start the execution of the instances on the cluster by running
```bash
./start.sh
```
5. After the execution has finished you can compress the output by running
```bash
./compress.sh
```
6. Copy the results from the server to process them locally or use a script to process the data on the server