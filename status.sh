#!/bin/bash

# Print some info on the status of the execution of the experiments

if test -f "pid"; then
    pid=$(<pid)
    if [ "$(ps -p $pid -o comm=)" = "runner.sh" ]; then
        echo "Runner is running"
    else
        echo "Runner is NOT running"
    fi
    else
    echo "Runner is NOT running"
fi

echo

echo "Total number benchmarks:" $(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT COUNT(*) FROM benchmarks")
echo "Not queued benchmarks:" $(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT COUNT(*) FROM benchmarks WHERE status = 'READY'")
echo "Pending/Running benchmarks:" $(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT COUNT(*) FROM benchmarks WHERE status = 'RUNNING'")
echo "Completed benchmarks:" $(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT COUNT(*) FROM benchmarks WHERE status = 'COMPLETED'")
echo "Failed benchmarks:" $(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT COUNT(*) FROM benchmarks WHERE NOT status = 'COMPLETED' AND NOT status = 'RUNNING' AND NOT status = 'READY'")
