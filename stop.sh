#!/bin/bash

# Stop the runner to schedule slurm jobs

pid=$(<pid)
kill -15 $pid
echo "Job scheduling stopped!"