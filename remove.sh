#!/bin/bash

# Add instance with script to run to database

directory=${2}
pattern=${3}
if [ -z $pattern ]
then
    files=$(find $directory -type f)
else
    files=$(find $directory -type f -name "$pattern")
fi
script=$PWD/${1}

for filename in $files
do
    sqlite3 benchmarks.db "DELETE FROM benchmarks WHERE instance = '$filename' AND script = '$script'"
    echo "Removed $filename with $script from the database!"
done