#!/bin/bash

# Compress output files and database to archive

tar -cjf results.tar.bz2 *.out *.db

echo "Output files and database compressed in results.tar.bz2"