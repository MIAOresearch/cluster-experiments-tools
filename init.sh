#!/bin/bash

# Initializes the database.

rm -f *.out
rm -f benchmarks.db
sqlite3 benchmarks.db "create table benchmarks(instance text, status text, script text, job_id int)"
echo "Database initialized!"