#!/bin/bash

# Default allocation of 1 compute node with maximum memory available (~14000 MB)
#SBATCH -N 1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=6
#SBATCH --mem-per-cpu=2333MB

# TODO: Change running time to be high enough for your timeout
#SBATCH -t 2:00:00

# TODO: Change name
#SBATCH -J nuc_job

# TODO: Set timout for execution
timeout=5000s

# TODO: Enter path to binary or add more variables if more binaries are used
binary_full_path=
binary_name=$(basename ${binary_full_path})

# TODO: Specify options to use for the execution of the binary
options=

instance_full_path=$1
instance_name=$(basename ${instance_full_path})

# copy binary and instances to compute node
cp -p $instance_full_path $binary_full_path $SNIC_TMP

cd $SNIC_TMP

echo "Metadata:"
echo "Instance: $instance_name"
echo "Hash: $(sha1sum $instance_name | awk '{print $1}')"
echo "Options: $options"
echo "Node: $(uname -n)"

echo "Run: "$binary_name $options $instance_name

start=`date +%s%N`
timeout $timeout $binary_name $options $instance_name
end=`date +%s%N`

echo Time: `expr $end - $start`

echo "end"
