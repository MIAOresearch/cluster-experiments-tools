#!/bin/bash

# Periodically check if new jobs can be queued and queue them.

MAX_JOBS_PENDING=150 # This value should be safe to go, but can be adjusted.

while :
do
    # Check status of running/completed jobs
    running_ids=$(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT job_id FROM benchmarks WHERE status = 'RUNNING'")
    for id in $running_ids
    do
        state=$(sacct --format State -j $id | tail -n 1 | awk '{print $1}')
        case $state in
            "RUNNING")
                true;;
            "PENDING")
                true;;
            "COMPLETING")
                true;;
            "TIMEOUT")
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'TIMEOUT' WHERE job_id = $id";;
            "CANCELLED")
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'CANCELLED' WHERE job_id = $id";;
            "COMPLETED")
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'COMPLETED' WHERE job_id = $id";;
            "FAILED")
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'FAILED' WHERE job_id = $id";;
            "OUT_OF_ME+")
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'OUT_OF_MEMORY' WHERE job_id = $id";;
            *)
                sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'UNKNOWN' WHERE job_id = $id";;
        esac
    done

    # Schedule jobs if there is room on the queue
    jobs_pending=$(squeue -t PD -u $USER | wc | awk '{print $1}')
    for (( ; jobs_pending<=MAX_JOBS_PENDING; jobs_pending++ ))
    do
        script=$(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT script FROM benchmarks WHERE status = 'READY' ORDER BY ROWID ASC LIMIT 1")
        if [ -z "$script" ]
        then
            break
        fi
        instance=$(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT instance FROM benchmarks WHERE status = 'READY' ORDER BY ROWID ASC LIMIT 1")
        id=$(sbatch --parsable --exclude=nuc96 -p nuc $script $instance)
        sqlite3 -cmd ".timeout 30000" benchmarks.db "UPDATE benchmarks SET status = 'RUNNING', job_id = $id WHERE instance = '$instance' AND script = '$script'"
    done

    # Terminate if there are no more jobs running
    top_row_running=$(sqlite3 -cmd ".timeout 30000" benchmarks.db "SELECT * FROM benchmarks WHERE status = 'RUNNING' ORDER BY ROWID ASC LIMIT 1")
    if [ -z "$top_row_running" ]
    then
        break
    fi

    sleep 60 # This value should be safe to go, but can be adjusted.
done
