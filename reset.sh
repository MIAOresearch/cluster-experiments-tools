#!/bin/bash

# Reset database and deletes any slurm output in the directory

rm -f *.out
sqlite3 benchmarks.db "UPDATE benchmarks SET status = 'READY'"
echo "All database entries reset!"