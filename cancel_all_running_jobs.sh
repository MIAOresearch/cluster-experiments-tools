#!/bin/bash

# This script cancels all jobs running and scheduled on our cluster.

scancel -p nuc

echo All running and scheduled jobs stopped!
