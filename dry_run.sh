#!/bin/bash

script=$1
instance=$2
SLURM_SUBMIT_DIR=$PWD

mkdir tmp

SNIC_TMP=./tmp

./$script $instance

cd $SLURM_SUBMIT_DIR

rm -rf tmp