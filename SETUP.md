# How to Log In to the Cluster, Set It Up and Run an Interactive Job <!-- omit in toc -->

- [Setting Up a LUNARC Account](#setting-up-a-lunarc-account)
- [Login to the Cluster](#login-to-the-cluster)
- [Loading Dependencies and Libraries](#loading-dependencies-and-libraries)
- [Running an Interactive Session](#running-an-interactive-session)

These setup instructions are specific to our NUC cluster and assumes that you are already a member of the SNIC project for the NUC cluster. See also the [tutorial on the LUNARC website](https://lunarc-documentation.readthedocs.io/en/latest/) for more details on how to work with the cluster.

If you are already have a LUNARC account, go to [Login to the Cluster](#login-to-the-cluster), otherwise follwo the instructions below on how to get one. If you are already logged in to the cluster and want to run things, then have a look at [Loading Dependencies and Libraries](#loading-dependencies-and-libraries), [Running an Interactive Session](#running-an-interactive-session) or directly at the [README.md](README.md) on instructions how to run large scale experiments.

## Setting Up a LUNARC Account
These steps assume that you already have an account with [SUPR](https://supr.naiss.se/) (otherwise click on `Register New Person`) and you have requested access to a project.

1. Got to [https://supr.naiss.se/](https://supr.naiss.se/)
2. Login with your SNIC/SUPR account (normally through `Login with SWAMID`)
3. Go to [Accounts](https://supr.naiss.se/account/)
4. Press the button under Account Requests.
   Note: if there is no button there, then you first need to request membership in the project (search for NUC Cluster or Jakob Nordström).
5. Fill in the required details, here you also choose the LUNARC username, notice that you should be able to receive SMS with the registered phone number
6. Submit the request
7. To activate the account, you need a two-factor authenticator (2FA) app, any 2FA app does the job (e.g. [FreeOTP+](https://play.google.com/store/apps/details?id=org.liberty.android.freeotpplus)/[FreeOTP](https://freeotp.github.io/)), [LUNARC recommends Pocket Pass](https://lunarc-documentation.readthedocs.io/en/latest/getting_started/authenticator_howto/)
8. After some time (few minutes to a day) you should receive an email (**check your SPAM folder!!!**) with your LUNARC username and with a link to activate the account and set up the 2FA app
9. Follow [the link in the email](https://phenix3.lunarc.lu.se/pss)
10. Enter your email
11. You will receive an SMS with an OTP code, enter this OTP code
12. Assign a password to your LUNARC account
13. Go to [https://phenix3.lunarc.lu.se/selfservice/authenticate/unpwotp](https://phenix3.lunarc.lu.se/selfservice/authenticate/unpwotp) and login with your LUNARC account
14. You will receive a one-time-password (OTP) via SMS, enter this OTP code on the next screen
15. Go to the `TOKENS` tab
16. Click on `Activate PhenixID Pocket Pass`
17. Enter a name for this entry (e.g. `Cosmos`)
18. Scan the QR-code with your 2FA app
19. Click `Next`
20. Enter the OTP code from the 2FA app (not the code you received via SMS)
21. Done!

## Login to the Cluster

1. Open a terminal
2. Enter the following command, where `<username>` is replaced by your LUNARC username:
```bash
ssh <username>@cosmos.lunarc.lu.se
```
3. Enter your LUNARC password
4. Enter your LUNARC OTP code from the authenticator app
5. Done!

## Loading Dependencies and Libraries

For more information type:
```bash
module
```

The following commands are helpful:
- Searching for a module:
```bash
module spider <search-term>
```
- Loading a module:
```bash
module load <module-name>
```
- Saving the loaded modules as default modules loaded
```bash
module s
```

For example, to compile VeriPB run:
```bash
module load GCC
module load Python
```

Or to compile RoundingSAT run:
```bash
module load GCC/11.2.0
module load CMake
module load Boost
```

## Running an Interactive Session

The following instructions start an interactive session on a NUC node of the cluster. This can be helpful for figuring out how to run things on the nodes and all software should be compiled on the nodes for the best performance in benchmarks.

1. We assume that you are logged in to the cluster using the steps in [Login to the Cluster](#login-to-the-cluster)
2. Use the following command to start the session
```bash
interactive -p nuc -t <time-of-session>
```
3. It could take some time to start the job
4. Enter your LUNARC password (recently, connecting to the node worked without a password)
5. Sometimes you have to load modules again, you can use the following command to restore your default modules
```bash
module r
```
