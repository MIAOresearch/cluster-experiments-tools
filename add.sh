#!/bin/bash

# Add instance with script to run to database

directory=${2}
pattern=${3}
if [ -z $pattern ]
then
    files=$(find $directory -type f)
else
    files=$(find $directory -type f -name "$pattern")
fi
script=$PWD/${1}

for filename in $files
do
    num_appearances_in_db=$(sqlite3 benchmarks.db "select * from benchmarks where instance = '$filename' and script = '$script'" | wc | awk '{print $1}')
    if [ $num_appearances_in_db == 0 ]
    then
        sqlite3 benchmarks.db "insert into benchmarks values('$filename', 'READY', '$script', NULL)"
        echo "Added $filename with $script to database!"
    fi
done