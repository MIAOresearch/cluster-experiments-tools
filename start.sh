#!/bin/bash

# Start the runner to schedule slurm jobs

nohup ./runner.sh &> runner_output.txt & echo $! > pid
echo "Job scheduling started!"

