#!/bin/bash

# Unzip instances that are compressed with gzip or bzip2, or leave them untouched if they are not compressed

filename=$1

extension=${filename##*.}

case $extension in
    "bz2")
        bzip2 -d $filename
        filename="${filename%.*}";;
    "gz")
        gzip -d $filename
        filename="${filename%.*}";;
    "xz")
        xz --decompress $filename
        filename="${filename%.*}";;
esac

echo $filename
